var mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.scripts([
    'resources/assets/js/jquery.min.js',
    'resources/assets/js/jquery-ui.js',
    'resources/assets/js/adminlte.min.js',
    'resources/assets/js/bootstrap.min.js',
    /*'resources/assets/plugins/datePicker/js/bootstrap-datepicker.min.js',
    'resources/assets/plugins/bootBox/js/bootbox.min.js',
    'resources/assets/plugins/toastr/js/toastr.min.js',
    'resources/assets/plugins/dateTimePicker/js/jquery.datetimepicker.full.min.js',
    'resources/assets/plugins/bsvalidator_master/js/bootstrapValidator.min.js',
    'resources/assets/plugins/bootstrap_select/js/bootstrap_select.min.js',
    'resources/assets/plugins/select2/js/select2.min.js',
    'resources/assets/plugins/fullCalender/js/fullcalendar.min.js',*/
], 'public/js/vendor.js')
    .styles([
        'resources/assets/css/bootstrap.min.css',
        'resources/assets/css/AdminLTE.min.css',
        'resources/assets/css/skins/_all-skins.min.css',
        'resources/assets/css/font-awesome.min.css',
        'resources/assets/css/ioniocons.min.css',
        'resources/assets/css/social_styles.css',
        'resources/assets/css/material-icons.css',
        /*'resources/assets/css/sales_admin_styles.css',
        'resources/assets/css/login_page_styles.css',
        'resources/assets/plugins/datePicker/css/bootstrap-datepicker.min.css',
        'resources/assets/plugins/toastr/css/toastr.min.css',
        'resources/assets/plugins/dateTimePicker/css/jquery.datetimepicker.min.css',
        'resources/assets/plugins/bsvalidator_master/css/bootstrapValidator.min.css',
        'resources/assets/plugins/bootstrap_select/css/bootstrap_select.min.css',
        'resources/assets/plugins/select2/css/select2.min.css',
        'resources/assets/plugins/fullCalender/css/fullcalendar.min.css',*/
    ], 'public/css/all.css');

mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css');
