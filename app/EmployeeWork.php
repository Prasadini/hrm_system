<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeWork extends Model
{

    protected $table = "emp_work";
    protected $primaryKey = "work_id";
}
