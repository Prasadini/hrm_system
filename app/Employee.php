<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    public $fillable = ['firstname', 'middle_name', 'lastname',
        'email_address', 'emp_status', 'emp_type', 'hired_date'];
    protected $table = "employee";
    protected $primaryKey = "emp_id";
}
