<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeContact extends Model
{
    protected $table = "emp_contacts";
    protected $primaryKey = "contact_id";
}
