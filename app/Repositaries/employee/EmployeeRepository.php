<?php
/**
 * Created by PhpStorm.
 * User: Chaththa
 * Date: 7/21/2019
 * Time: 4:20 PM
 */

namespace App\Repositaries\employee;


use App\Employee;
use App\EmployeeContact;
use App\EmployeeWork;
use App\User;
use Illuminate\Support\Facades\Hash;

class EmployeeRepository
{
    public function savePersonalDetails($customer_data, $data_id)
    {

        $emp_personal_details = Employee::find($data_id);
        $emp_personal_details->full_name = $customer_data['full_name'];
        $emp_personal_details->dob = $customer_data['dob'];
        $emp_personal_details->nationality = $customer_data['nationality'];
        $emp_personal_details->gender = $customer_data['gender'];
        $emp_personal_details->marital_status = $customer_data['marital_status'];
        $emp_personal_details->nic_number = $customer_data['nic_number'];
        $emp_personal_details->address = $customer_data['address'];
        $emp_personal_details->city_id = $customer_data['city'];
        $emp_personal_details->province_id = $customer_data['province'];
        $emp_personal_details->country_id = $customer_data['country'];
        $emp_personal_details->postal_code = $customer_data['postal_code'];
        $emp_personal_details->save();

    }

    public function saveWorkDetails($customer_data,$emp_id)
    {
        $new_customer = new EmployeeWork();
        $new_customer->emp_id = $emp_id;
        $new_customer->company_id = isset($customer_data['company_id'])? $customer_data['company_id']: 1;
        $new_customer->designation = $customer_data['job_title'];
        $new_customer->period = isset($customer_data['period'])? $customer_data['period']: 1;
//        $new_customer->location = $customer_data['location'];
        $new_customer->description = isset($customer_data['description'])? $customer_data['description']: 1;
        $new_customer->department = $customer_data['department'];
        $new_customer->reporting_to = $customer_data['reporting_to'];
        $new_customer->source_of_hire = $customer_data['source_of_hire'];
        $new_customer->pay_rate = $customer_data['pay_rate'];
        $new_customer->save();
        return $new_customer->work_id;
    }
    public function saveEmployeeContact($phone_numbers,$emp_id){
        foreach($phone_numbers as $number){
            $new_contact = new EmployeeContact();
            $new_contact->contact = $number;
            $new_contact->type = 2;
            $new_contact->level = 2;
            $new_contact->emp_id = $emp_id;
            $new_contact->save();
        }

    }
    public function createNewUser($emp_id,$job_id){

        $new_user = new User();
        $new_user->user_name = $emp_id;
        $new_user->password = Hash::make('123456');
        $new_user->emp_id = $emp_id;
        $new_user->role = 1;
        $new_user->save();
        return $new_user->id;
    }
}