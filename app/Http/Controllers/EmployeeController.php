<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Repositaries\employee\EmployeeRepository;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
    private $employee;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(EmployeeRepository $employee)
    {
        $this->employee = $employee;
    }

    public function index()
    {
        return view('pages.employee.add_employee');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $customer_data = $request->all();
            $is_work_data = $customer_data['add_work_details'];
            $is_personal_data = $customer_data['add_personal_details'];
            $work_data = $customer_data['work_form'];
            $personal_data = $customer_data['personal_form'];
            $send_welcome_email = $customer_data['send_welcome_email'];
            $send_login_details = $customer_data['send_login_details'];

           $new_employee = new Employee();
           $new_employee->firstname = $customer_data['first_name'];
           $new_employee->middle_name = $customer_data['middle_name'];
           $new_employee->lastname = $customer_data['last_name'];
           $new_employee->email_address = $customer_data['email_address'];
           $new_employee->emp_status = $customer_data['employee_status'];
           $new_employee->emp_type = $customer_data['employee_type'];
           $new_employee->hired_date = $customer_data['date_of_hire'];
           $new_employee->save();

            if ($is_personal_data) {
                $this->employee->savePersonalDetails($personal_data,$new_employee->emp_id);
            }
            if ($is_work_data) {
                $this->employee->saveWorkDetails($work_data,$new_employee->emp_id);
            }
            if(!empty($phone_numbers)){
                $this->employee->saveEmployeeContact($phone_numbers,$new_employee->emp_id);
            }
            /*: [],
            secondary_email: '',*/
            $this->employee->createNewUser($new_employee->emp_id,1);


            return response()->json(['success' => true, 'message' => 'Employee created!']);


        } catch (QueryException $e) {
            \Log::alert($e);
            $response = [
                'success' => false,
                'msg' => $e->getMessage(),
                'message' => 'Something went wrong.!!'
            ];
            return response()->json($response);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
