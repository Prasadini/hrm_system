<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Auth;

class MainMenuController extends Controller
{
    public function getMenuDetails()
    {
        $side_menu = \DB::table('side_menu_details')
            ->select('menu_name', 'menu_master_id', 'menu_level', 'menu_url', 'menu_id', 'alias', 'menu_icon')
            ->where('status', 1)
            ->orderBy('sort_order', 'ASC')
            ->get();

        return response()->json(['side_menu' => $side_menu]);
    }
}
