<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Input;
class LoginController extends Controller
{
    public function showLogin(){
        return view('login');
    }
    public function userLogin(Request $request){
        try {
            $validatedData = Validator::make($request->all(), [
                'user_name' => 'required|max:10',
                'password' => 'required|max:8|min:6',
            ]);

            if ($validatedData->fails()) {
                return Redirect::to('login')->withErrors($validatedData)->withInput(Input::except('password'));
            } else {
                $credentials['user_name'] = $request->user_name;
                $credentials['password'] = $request->password;
                $remember_me = $request->remember_me;
                if (Auth::attempt($credentials)) {
                    $request->session()->put('user_id', Auth::id());
                    return Redirect::to('/employees');
                } else {
                    return Redirect::to('/login')->with('message','Invalid Credentials.Try again!');

                }
            }

        } catch (\Exception $e) {
            \Log::alert($e);
            return Redirect::to('login')->with('message','Something went wrong.Try again!');
        }

    }
    public function doLogout(Request $request)
    {

        try {
            Auth::Logout();
            return Redirect::to('login');
        } catch (\Exception $e) {
            \Log::alert($e);
            return response()->json(['success' => false, 'message' => 'Some thing went wrong!']);
        }
    }
}
