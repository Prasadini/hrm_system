<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- Styles -->
    <style>

        /* BASIC */

        html {
            background-color: #eae3ed;
        }

        body {
            font-family: "Poppins", sans-serif;
            height: 100vh;
        }

        a {
            color: #92badd;
            display: inline-block;
            text-decoration: none;
            font-weight: 400;
        }

        h2 {
            text-align: center;
            font-size: 16px;
            font-weight: 600;
            text-transform: uppercase;
            display: inline-block;
            margin: 40px 8px 10px 8px;
            color: #cccccc;
        }

        /* STRUCTURE */

        .wrapper {
            display: flex;
            align-items: center;
            flex-direction: column;
            justify-content: center;
            width: 100%;
            min-height: 100%;
            padding: 20px;
            background: #222d32;
        }

        #formContent {
            background: #ffffff42;
            padding: 30px;
            width: 90%;
            max-width: 450px;
            position: relative;
            -webkit-box-shadow: 0 30px 60px 0 rgba(0, 0, 0, 0.3);
            box-shadow: 0 30px 60px 0 rgba(0, 0, 0, 0.3);
            /*text-align: center;*/
            /*font-size: 13px;*/
        }

        /* OTHERS */

        *:focus {
            outline: none;
        }

        #icon {
            width: 60%;
        }

        /* Bordered form */
        form {
            border: 3px solid #f1f1f1;
        }

        /* Full-width inputs */
        input[type=text], input[type=password] {
            width: 100%;
            padding: 8px 12px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            box-sizing: border-box;
           /* font-size: 13px;*/
        }

        /* Set a style for all buttons */
        button {
            background-color: #f39c12;
            color: white;
            padding: 8px 20px;
            margin: 8px 0;
            border: none;
            cursor: pointer;
            width: 100%;
        }

        /* Add a hover effect for buttons */
        button:hover {
            opacity: 0.8;
        }

        /* Add padding to containers */
        .container {
            padding: 16px;
        }

        /* The "Forgot password" text */
        span.psw {
            border-left: 1px solid white;
            padding-left: 5px;
        }

        /* Change styles for span and cancel button on extra small screens */
        @media screen and (max-width: 300px) {
            span.psw {
                display: block;
                float: none;
            }
        }

        .login-heading {
            color: white;
            padding: 25px;
            margin-bottom: .5rem;
            font-family: inherit;
            font-weight: 500;
            line-height: 1.2;
        }
        .error-text {
            color: #ff4700;
        }
        .error-field{
            border: 1px solid #ff0000 !important;
            box-shadow: 0 1px 1px 0 #ff0000, 0 1px 8px 0 #ff0000d4;
        }

    </style>
</head>
<body>
<div class="wrapper">
    <div class="login-heading">
        <h1>EM System | Login</h1>
    </div>
    <div id="formContent">
        @if( session('message'))
        <h6 class="error-text text-center">{{session('message')}}</h6>
        @endif
        <form action="/user-login" method="POST">
            <div class="container">
                {{ csrf_field() }}
                <input type="text" placeholder="Enter Username" name="user_name" class="{{($errors->has('user_name')? 'error-field':'')}}">
                @if( $errors->has('user_name'))
                    <span class="error-text">{{ $errors->first('user_name') }}</span>
                @endif
                <input type="password" placeholder="Enter Password" name="password" class="{{($errors->has('password')? 'error-field':'')}}">
                @if( $errors->has('password'))
                    <span class="error-text">{{ $errors->first('password') }}</span>
                @endif

                <button type="submit">Login</button>
                <div class="text-right">
                    <label>
                        <input type="checkbox" checked="checked" name="remember" class> Remember me
                    </label>
                    <span class="psw"><a href="#">Forgot  password?</a></span>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
