<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding fnt-13 text-left">

    <form class="form-inline text-left margin-top20">
        <h4 class="clr-black fnt-600">Balances</h4>
        <div>
            <table class="table table-bordered fnt-13">
                <thead class="back-gray">
                <tr>
                    <th>Leave</th>
                    <th>Current</th>
                    <th>Scheduled</th>
                    <th>Available</th>
                    <th>Period</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>General</td>
                    <td>20 days</td>
                    <td> -</td>
                    <td>20 days</td>
                    <td>25/04/2019 - 30/09-2019</td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>
    <hr>
    {{-- history--}}
    <div>
        <h4 class="clr-black fnt-600">History</h4>
        <div class="show-inline margin-bot15">
            <div class="width_12">
                <div class="form-group">
                    <select class="form-control">
                        <option>All policy</option>
                    </select>
                </div>
            </div>
            <div class="width_12">
                <div class="form-group">
                    <select class="form-control">
                        <option>2017</option>
                    </select>
                </div>
            </div>
            <div class="width_12">
                <button class="btn btn-sm btn-default" type="button">Filter</button>
            </div>
        </div>
        <br>
        <table class="table table-bordered fnt-13">
            <thead class="back-gray">
            <tr>
                <th>Date</th>
                <th>Policy</th>
                <th>Description</th>
                <th>Days</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>
    </div>


    <hr>
    {{--hr management--}}
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fnt-13 back-gray pad_10">
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
            <h4 class="clr-black fnt-600">HR Management</h4>
            <div class="back-white pad_10 margin_15">
                <h4>HR Announcement</h4>
                <p class="justify">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                    industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type
                    and scrambled it to make a type specimen book. It has survived not only five centuries, but also the
                    leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s
                    with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop
                    publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                </p>
            </div>
            <br>

            <div class="back-white pad_10 margin_15">
                <h4>My Leave Calender</h4>
                <button type="button" name="takeLeave" class="btn btn-primary">Take Leave</button>
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <div class="back-white pad_10 margin_15">
                <h4>Birthday Buddies</h4>
                <p class="justify">Upcoming Birthday</p>
                <h5>Chathurangika Rupasinghe <span class="text-right">Nov,28</span></h5>
            </div>
            <div class="back-white pad_10 margin_15">
                <h4>Who is out</h4>
                <p class="justify">No one is on vacation on this or next month</p>
                {{--<h5>Chathurangika Rupasinghe <span class="text-right">Nov,28</span></h5>--}}
            </div>
            <div class="back-white pad_10 margin_15">
                <h4>Attendance status</h4>
            </div>
            <div class="back-white pad_10 margin_15">
                <h4>Attendance self service</h4>
            </div>
        </div>
    </div>
</div>

