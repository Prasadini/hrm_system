@extends('layout.master')
@section('content')
    <div id="employeeDetails">
        {{--@include('_includes.page_header')--}}
        <section class="content-header" style="margin-bottom: 10px;">
            <h1>
                Employees
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <ul class="list-inline" style="font-size: 12px;">
                            <li><a href="#">All (250)</a></li>
                            <li><a href="#">Terminated (50)</a></li>
                            <li><a href="#">Resigned (5)</a></li>
                        </ul>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <div class="btn-group">
                                <button type="button" class="btn btn-sm btn-primary btn-create" data-toggle="modal"
                                        data-target="#myModal">Add New</button>
                                {{--<button type="button" class="btn btn-sm btn-primary btn-create">Change Job</button>--}}
                            </div>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-striped table-bordered table-list" style="font-size:13px;">
                            <thead>
                            <tr>
                                <th>Action</th>
                                <th class="hidden-xs">Employee Id</th>
                                <th>Calling Name</th>
                                <th>Department</th>
                                <th>Job</th>
                                <th>Employee Type</th>
                                <th>Contact No</th>
                                <th>Join date</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="(detail,index) in details" class="text-left">
                                <td align="center">
                                    <a title="Edit Employee" class="btn web-btn btn-default" href="/employee-profile"><em
                                                class="fa fa-pencil"></em></a>
                                    <a title="Delete Employee" class="btn web-btn btn-danger" data-toggle="modal"
                                       data-target="#employeeDeleteModal"><em class="fa fa-trash"></em></a>
                                    <a href="/employee-profile" title="Preview Employee Profile" class="btn web-btn btn-info"><em class="fa fa-eye"></em></a>
                                    <a title="Change Job" class="btn web-btn btn-info" data-toggle="modal"
                                       data-target="#changeJobModal"><em class="fa fa-briefcase"></em></a>
                                </td>
                                <td class="hidden-xs">@{{ detail.id }}</td>
                                <td>@{{ detail.name }}</td>
                                <td>@{{ detail.phone }}</td>
                                <td>@{{ detail.join }}</td>
                                <td>@{{ detail.status }}</td>
                                <td>@{{ detail.status }}</td>
                                <td>@{{ detail.status }}</td>
                                <td><span class="label label-success">Approved</span></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <ul class="pagination pagination-sm no-margin pull-right">
                            <li><a href="#">«</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">»</a></li>
                        </ul>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
        </div>
        <div class="container">
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h5 class="modal-title">Edit info</h5>
                        </div>
                        <div class="modal-body">
                            <div>
                                <div class="row">
                                    <div class="col-sm-3"><!--left col-->


                                        <div class="text-center">
                                            <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png"
                                                 class="avatar img-circle img-thumbnail" alt="avatar">
                                            <h6>Upload a different photo...</h6>
                                            <input type="file" class="text-center center-block file-upload">
                                        </div>
                                        </hr><br>


                                        <div class="panel panel-default">
                                            <div class="panel-heading">Website <i class="fa fa-link fa-1x"></i>
                                            </div>
                                            <div class="panel-body"><a href="http://bootnipets.com">bootnipets.com</a>
                                            </div>
                                        </div>


                                    </div><!--/col-3-->
                                    <div class="col-sm-9">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a data-toggle="tab" href="#home">General
                                                    Info</a></li>
                                            <li><a data-toggle="tab" href="#works">Work</a></li>
                                            <li><a data-toggle="tab" href="#personal">Personal Details</a></li>
                                        </ul>


                                        <div class="tab-content">
                                            <div class="tab-pane active" id="home">
                                                <hr>
                                                <form class="form" action="#" method="post"
                                                      id="registrationForm">
                                                    <div class="form-group">

                                                        <div class="col-xs-6">
                                                            <label for="first_name"><h5 class="pull-left">First
                                                                    name</h5></label>
                                                            <input type="text" class="form-control"
                                                                   name="first_name"
                                                                   id="first_name" placeholder="first name"
                                                                   title="enter your first name if any.">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">

                                                        <div class="col-xs-6">
                                                            <label for="last_name"><h5 class="pull-left">Last name</h5>
                                                            </label>
                                                            <input type="text" class="form-control"
                                                                   name="last_name"
                                                                   id="last_name" placeholder="last name"
                                                                   title="enter your last name if any.">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">

                                                        <div class="col-xs-6">
                                                            <label for="phone"><h5 class="pull-left">Phone</h5></label>
                                                            <input type="text" class="form-control" name="phone"
                                                                   id="phone"
                                                                   placeholder="enter phone"
                                                                   title="enter your phone number if any.">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-xs-6">
                                                            <label for="mobile"><h5 class="pull-left">Mobile</h5>
                                                            </label>
                                                            <input type="text" class="form-control"
                                                                   name="mobile"
                                                                   id="mobile" placeholder="enter mobile number"
                                                                   title="enter your mobile number if any.">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">

                                                        <div class="col-xs-6">
                                                            <label for="email"><h5 class="pull-left">Email</h5></label>
                                                            <input type="email" class="form-control"
                                                                   name="email" id="email"
                                                                   placeholder="you@email.com"
                                                                   title="enter your email.">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">

                                                        <div class="col-xs-6">
                                                            <label for="email"><h5 class="pull-left">Location</h5>
                                                            </label>
                                                            <input type="email" class="form-control"
                                                                   id="location"
                                                                   placeholder="somewhere"
                                                                   title="enter a location">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">

                                                        <div class="col-xs-6">
                                                            <label for="password"><h5 class="pull-left">Password</h5>
                                                            </label>
                                                            <input type="password" class="form-control"
                                                                   name="password"
                                                                   id="password" placeholder="password"
                                                                   title="enter your password.">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">

                                                        <div class="col-xs-6">
                                                            <label for="password2"><h5 class="pull-left">Verify</h5>
                                                            </label>
                                                            <input type="password" class="form-control"
                                                                   name="password2"
                                                                   id="password2" placeholder="password2"
                                                                   title="enter your password2.">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <br>
                                                            <button class="btn btn-sm btn-success"
                                                                    type="submit">
                                                                Save
                                                            </button>
                                                            <button class="btn btn-sm" type="reset">
                                                                Reset
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>

                                                <hr>

                                            </div><!--/tab-pane-->
                                            <div class="tab-pane" id="works">
                                                <hr>
                                                <form class="form" action="#" method="post"
                                                      id="registrationForm">
                                                    <div class="form-group">

                                                        <div class="col-xs-6">
                                                            <label for="first_name"><h5 class="pull-left">Job</h5>
                                                            </label>
                                                            <input type="text" class="form-control"
                                                                   name="first_name"
                                                                   id="first_name" placeholder="first name"
                                                                   title="enter your first name if any.">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">

                                                        <div class="col-xs-6">
                                                            <label for="last_name"><h5 class="pull-left">Title</h5>
                                                            </label>
                                                            <input type="text" class="form-control"
                                                                   name="last_name"
                                                                   id="last_name" placeholder="last name"
                                                                   title="enter your last name if any.">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">

                                                        <div class="col-xs-6">
                                                            <label for="phone"><h5 class="pull-left">Phone</h5></label>
                                                            <input type="text" class="form-control" name="phone"
                                                                   id="phone"
                                                                   placeholder="enter phone"
                                                                   title="enter your phone number if any.">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-xs-6">
                                                            <label for="mobile"><h5 class="pull-left">Mobile</h5>
                                                            </label>
                                                            <input type="text" class="form-control"
                                                                   name="mobile"
                                                                   id="mobile" placeholder="enter mobile number"
                                                                   title="enter your mobile number if any.">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">

                                                        <div class="col-xs-6">
                                                            <label for="email"><h5 class="pull-left">Email</h5></label>
                                                            <input type="email" class="form-control"
                                                                   name="email" id="email"
                                                                   placeholder="you@email.com"
                                                                   title="enter your email.">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">

                                                        <div class="col-xs-6">
                                                            <label for="email"><h5 class="pull-left">Location</h5>
                                                            </label>
                                                            <input type="email" class="form-control"
                                                                   id="location"
                                                                   placeholder="somewhere"
                                                                   title="enter a location">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">

                                                        <div class="col-xs-6">
                                                            <label for="password"><h5 class="pull-left">Password</h5>
                                                            </label>
                                                            <input type="password" class="form-control"
                                                                   name="password"
                                                                   id="password" placeholder="password"
                                                                   title="enter your password.">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">

                                                        <div class="col-xs-6">
                                                            <label for="password2"><h5 class="pull-left">Verify</h5>
                                                            </label>
                                                            <input type="password" class="form-control"
                                                                   name="password2"
                                                                   id="password2" placeholder="password2"
                                                                   title="enter your password2.">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <br>
                                                            <button class="btn btn-sm btn-success"
                                                                    type="submit">
                                                                Save
                                                            </button>
                                                            <button class="btn btn-sm" type="reset">
                                                                Reset
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>

                                            </div><!--/tab-pane-->
                                            <div class="tab-pane" id="personal">
                                                <hr>
                                                <form class="form" action="#" method="post"
                                                      id="registrationForm">
                                                    <div class="form-group">

                                                        <div class="col-xs-6">
                                                            <label for="first_name"><h5 class="pull-left">Address
                                                                    1 </h5></label>
                                                            <input type="text" class="form-control"
                                                                   name="first_name"
                                                                   id="first_name" placeholder="first name"
                                                                   title="enter your first name if any.">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">

                                                        <div class="col-xs-6">
                                                            <label for="last_name"><h5 class="pull-left">Address 2</h5>
                                                            </label>
                                                            <input type="text" class="form-control"
                                                                   name="last_name"
                                                                   id="last_name" placeholder="last name"
                                                                   title="enter your last name if any.">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">

                                                        <div class="col-xs-6">
                                                            <label for="phone"><h5 class="pull-left">City</h5></label>
                                                            <input type="text" class="form-control" name="phone"
                                                                   id="phone"
                                                                   placeholder="enter phone"
                                                                   title="enter your phone number if any.">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-xs-6">
                                                            <label for="mobile"><h5 class="pull-left">Mobile</h5>
                                                            </label>
                                                            <input type="text" class="form-control"
                                                                   name="mobile"
                                                                   id="mobile" placeholder="enter mobile number"
                                                                   title="enter your mobile number if any.">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">

                                                        <div class="col-xs-6">
                                                            <label for="email"><h5 class="pull-left">Email</h5></label>
                                                            <input type="email" class="form-control"
                                                                   name="email" id="email"
                                                                   placeholder="you@email.com"
                                                                   title="enter your email.">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">

                                                        <div class="col-xs-6">
                                                            <label for="email"><h5 class="pull-left">Location</h5>
                                                            </label>
                                                            <input type="email" class="form-control"
                                                                   id="location"
                                                                   placeholder="somewhere"
                                                                   title="enter a location">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">

                                                        <div class="col-xs-6">
                                                            <label for="password"><h5 class="pull-left">Password</h5>
                                                            </label>
                                                            <input type="password" class="form-control"
                                                                   name="password"
                                                                   id="password" placeholder="password"
                                                                   title="enter your password.">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">

                                                        <div class="col-xs-6">
                                                            <label for="password2"><h5 class="pull-left">Verify</h5>
                                                            </label>
                                                            <input type="password" class="form-control"
                                                                   name="password2"
                                                                   id="password2" placeholder="password2"
                                                                   title="enter your password2.">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <br>
                                                            <button class="btn btn-sm btn-success"
                                                                    type="submit">
                                                                Save
                                                            </button>
                                                            <button class="btn btn-sm" type="reset">
                                                                Reset
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                        </div><!--/tab-pane-->
                                    </div><!--/tab-content-->

                                </div><!--/col-9-->
                            </div><!--/row-->

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{--delete modal--}}
        <div class="modal fade" id="employeeDeleteModal" role="dialog">
            <div class="modal-dialog modal-danger modal-sm">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title">Confirmation</h5>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure want to delete this employee?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        <!-- Change JOb Modal -->
        <div id="changeJobModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Change Job</h4>
                    </div>
                    <form action="/action_page.php">
                    <div class="modal-body">

                            <div class="form-group">
                                <label for="employee_id">Employee id:</label>
                                <input type="text" class="form-control" id="employee_id">
                            </div>
                            <div class="form-group">
                                <label for="department">Department:</label>
                                <select class="form-control" id="department">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="job">Job:</label>
                                <select class="form-control" id="job">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="job description">Job description:</label>
                                <textarea class="form-control" rows="5" id="job description"></textarea>
                            </div>


                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-default">Submit</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        let employeeDetailsApp = new Vue({
            el: "#employeeDetails",
            name: "employeeDetailsApp",
            data: {
                details: [{
                    id: 1,
                    name: 'Chaththa',
                    phone: +712369857,
                    join: '11/11/2018',
                    status: 1
                }]

            },
            computed: {},
            watch: {}
        })
    </script>
@endpush
<style>
    .web-btn {
        padding: 3px 6px !important;
    }

    #employeeDetails .box {
        border: none;
    }
</style>