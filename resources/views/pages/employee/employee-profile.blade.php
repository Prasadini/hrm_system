@extends('layout.master')
@section('content')
    @include('_includes.page_header')
    <div class="panel panel-default" v-cloak>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="media">
                        <div class="media-left">
                            <div class="user-avater">
                                <img src="img/user-placeholder.png"
                                     class="img-def">
                            </div>
                            <div class="show-inline">
                                <button type="button" class="btn btn-info fnt-13 margin-right5"><i
                                            class="material-icons icon">
                                        cloud_upload
                                    </i>upload Picture
                                </button>
                                <button type="button" class="btn btn-info fnt-13">Cancel
                                </button>
                            </div>
                        </div>
                        <div class="media-body text-left">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <h4 class="media-heading">Jane M. Bailey</h4>
                                <h5>Designer</h5>
                                <h5><a href="">jane@yahoo.com</a></h5>
                                <h5>+9481 2245139</h5>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 no-ritpad text-right">
                                <div>
                                    <b>Actions</b>
                                    <div class="btn-group btn-group-sm">
                                        <button type="button" class="btn btn-success btn-sm">Edit
                                        </button>
                                        <button type="button" class="btn btn-danger btn-sm">Delete</button>
                                        <button type="button" class="btn btn-default btn-sm">Print</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>

                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#home">General Info</a></li>
                            <li><a data-toggle="tab" href="#menu1">Job</a></li>
                            <li><a data-toggle="tab" href="#menu2">Leave</a></li>
                            <li><a data-toggle="tab" href="#menu3">Payroll</a></li>
                        </ul>
                        <div class="tab-content custom-tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <div> @include('pages.employee.general')</div>
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                <div> @include('pages.employee.job')</div>
                            </div>
                            <div id="menu2" class="tab-pane fade">
                                <div> @include('pages.employee.leave')</div>
                            </div>
                            <div id="menu3" class="tab-pane fade">
                                <div> @include('pages.employee.payroll')</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
<style>
    .custom-tab-content {
        background: #f1f1f1;
        padding: 10px;
        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
    }

    .no-top-border {
        border-top: none !important;
        margin-bottom: 5px !important;
    }
</style>

