<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-ritpad fnt-13">
        <div class="box no-top-border">
            <div class="box-header with-border">
                <h5 class="clr-black fnt-600 no-margin text-left">Details information at a glance</h5>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- box-header -->
            <div class="box-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-leftpad fnt-13">
                    <h4 class="fnt-600">Fixed Allowance </h4>
                    <table class="table table-striped fnt-13 no-margin">
                        <tbody>
                        <tr>
                            <th class="width_25">Pay Item</th>
                            <th class="text-center">Accommodation Allowance</th>
                        </tr>
                        <tr>
                            <td class="width_25">Pay Item Amount</td>
                            <td class="text-center">1000
                                <i class="material-icons pull-right">
                                    delete
                                </i></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="box-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-leftpad fnt-13">
                    <h4 class="fnt-600">Fixed Deduction </h4>
                    <table class="table table-striped fnt-13 no-margin">
                        <tbody>
                        <tr>
                            <th class="width_25">Pay Item</th>
                            <th class="text-center">Accommodation Allowance</th>
                        </tr>
                        <tr>
                            <td class="width_25">Pay Item Amount</td>
                            <td class="text-center">1000
                                <i class="material-icons pull-right">
                                    delete
                                </i>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="box-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-leftpad fnt-13">
                    <h4 class="fnt-600">Fixed Tax </h4>
                    <table class="table table-striped fnt-13 no-margin">
                        <tbody>
                        <tr>
                            <th class="width_25">Tax caption</th>
                            <th class="text-center">Accommodation Allowance</th>
                        </tr>
                        <tr>
                            <td class="width_25">Tax Amount</td>
                            <td class="text-center">1000
                                <i class="material-icons pull-right">
                                    delete
                                </i>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 fnt-13">
        <div class="box no-top-border">
            <div class="box-header with-border">
                <h5 class="clr-black fnt-600 no-margin text-left">Payroll basic and tax Info</h5>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-leftpad fnt-13">
                    <form class="form-inline text-left margin-top20">
                        <div class="row margin-bot10">
                            <label class="col-xs-5 col-sm-5 col-md-5 col-lg-5" for="taxId">Employee tax number:</label>
                            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                <input type="text" id="taxId" class="form-control">
                            </div>
                        </div>
                        <div class="row margin-bot10">
                            <label class="col-xs-5 col-sm-5 col-md-5 col-lg-5" for="basicPay">Basic pay:</label>
                            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                <input type="text" id="basicPay" class="form-control">
                            </div>
                        </div>
                        <div class="row margin-bot10">
                            <label class="col-xs-5 col-sm-5 col-md-5 col-lg-5" for="accNo">Bank Account Number:</label>
                            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                <input type="text" id="accNo" class="form-control">
                            </div>
                        </div>
                        <div class="row margin-bot10">
                            <label class="col-xs-5 col-sm-5 col-md-5 col-lg-5" for="accName">Bank Account Name:</label>
                            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                <input type="text" id="accName" class="form-control">
                            </div>
                        </div>
                        <div class="row margin-bot10">
                            <label class="col-xs-5 col-sm-5 col-md-5 col-lg-5" for="bankName">Bank Number:</label>
                            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                <input type="text" id="bankName" class="form-control">
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>

        <div class="box no-top-border">
            <div class="box-header with-border">
                <h5 class="clr-black fnt-600 no-margin text-left">Fix Allowance Payments</h5>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-leftpad fnt-13">
                    <form class="form-inline text-left margin-top20">
                        <div class="row margin-bot10">
                            <label class="col-xs-5 col-sm-5 col-md-5 col-lg-5" for="payItem">Pay Item:</label>
                            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                <select class="form-control" id="payItem">
                                    <option>Accommodation Allowance</option>
                                </select>
                            </div>
                        </div>
                        <div class="row margin-bot10">
                            <label class="col-xs-5 col-sm-5 col-md-5 col-lg-5" for="accNo">Pay Item Payment:</label>
                            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                <input type="text" id="payItemPayment" class="form-control">
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>

        <div class="box no-top-border">
            <div class="box-header with-border">
                <h5 class="clr-black fnt-600 no-margin text-left">Fixed Deduction Payments</h5>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-leftpad fnt-13">
                    <form class="form-inline text-left margin-top20">
                        <div class="row margin-bot10">
                            <label class="col-xs-5 col-sm-5 col-md-5 col-lg-5" for="deductionItem">Deduction
                                Item:</label>
                            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                <select class="form-control" id="deductionItem">
                                    <option>Advance</option>
                                </select>
                            </div>
                        </div>
                        <div class="row margin-bot10">
                            <label class="col-xs-5 col-sm-5 col-md-5 col-lg-5" for="deductionPayment">Deduction
                                Payment:</label>
                            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                <input type="text" id="deductionPayment" class="form-control">
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>

        <div class="box no-top-border">
            <div class="box-header with-border">
                <h5 class="clr-black fnt-600 no-margin text-left">Tax</h5>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-leftpad fnt-13">
                    <form class="form-inline text-left margin-top20">
                        <div class="row margin-bot10">
                            <label class="col-xs-5 col-sm-5 col-md-5 col-lg-5" for="tax">Tax:</label>
                            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                <select class="form-control" id="tax">
                                    <option>Income Tax</option>
                                </select>
                            </div>
                        </div>
                        <div class="row margin-bot10">
                            <label class="col-xs-5 col-sm-5 col-md-5 col-lg-5" for="taxAmount">Tax Amount:</label>
                            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                <input type="text" id="taxAmount" class="form-control">
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>

        <div class="box no-top-border">
            <div class="box-header with-border">
                <h5 class="clr-black fnt-600 no-margin text-left">Payment Details</h5>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-leftpad fnt-13">
                    <form class="form-inline text-left margin-top20">
                        <div class="row margin-bot10">
                            <label class="col-xs-5 col-sm-5 col-md-5 col-lg-5" for="paymentMethod">Payment
                                Method:</label>
                            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                <select class="form-control" id="paymentMethod">
                                    <option>Bank</option>
                                </select>
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-success">Submit Payment Info</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
