<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding fnt-13 text-left">

    <form class="form-inline text-left margin-top20">
        <h4 class="clr-black fnt-600">Employee Main status </h4>
        <div class="form-group">
            <label for="mainStatus">Employee status :</label>
            <select class="form-control" id="mainStatus">
                <option>Active</option>
                <option>Terminated</option>
                <option>Deceased</option>
                <option>Resigned</option>
            </select>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>

        {{-- status--}}
        <div>
            <div class="show-inline">
                <div class="width_75"><h4 class="clr-black fnt-600">Employment status </h4></div>
                <div class="width_25">
                    <button type="button" class="btn btn-default pull-right" data-toggle="modal"
                            data-target="#employmentStatusModal">Update status
                    </button>
                </div>
            </div>
            <table class="table table-bordered fnt-13">
                <thead class="back-gray">
                <tr>
                    <th>Date</th>
                    <th>Employee status</th>
                    <th>Comment</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>21/04/2015</td>
                    <td>Full time</td>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </div>

        <hr>
        {{-- compensation--}}
        <div>
            <div class="show-inline">
                <div class="width_75"><h4 class="clr-black fnt-600">Compensation </h4></div>
                <div class="width_25">
                    <button type="button" class="btn btn-default pull-right" data-toggle="modal"
                            data-target="#compensationModal">Update Compensation
                    </button>
                </div>
            </div>
            <table class="table table-bordered fnt-13">
                <thead class="back-gray">
                <tr>
                    <th>Date</th>
                    <th>Pay Rate</th>
                    <th>Pay Type</th>
                    <th>Change Reason</th>
                    <th>Comment</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>21/04/2015</td>
                    <td>Full time</td>
                    <td>Hourly</td>
                    <td>Performance</td>
                    <td>Based on performance</td>
                </tr>
                </tbody>
            </table>
        </div>

        {{-- info --}}
        <hr>

        <div>
            <div class="show-inline">
                <div class="width_75"><h4 class="clr-black fnt-600">Job Information </h4></div>
                <div class="width_25">
                    <button type="button" class="btn btn-default pull-right" data-toggle="modal"
                            data-target="#jobInfoModal">Update Job Information
                    </button>
                </div>
            </div>
            <table class="table table-bordered fnt-13">
                <thead class="back-gray">
                <tr>
                    <th>Date</th>
                    <th>Location</th>
                    <th>Department</th>
                    <th>Job Title</th>
                    <th>Report To</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>21/04/2015</td>
                    <td>Main</td>
                    <td>Designer</td>
                    <td>Designer</td>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </div>

    </form>

</div>

<!-- Employment status Modal -->
<div class="modal fade" id="employmentStatusModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Employment status</h4>
            </div>
            <div class="modal-body">
                <form class="text-left" action="">
                    <div class="row">
                        <label for="statusDate" class="col-xs-4 col-sm-4 col-md-4 col-lg-4">Date:</label>
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <input type="date" class="form-control" id="statusDate">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label for="status" class="col-xs-4 col-sm-4 col-md-4 col-lg-4">Employment status:</label>
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <select class="form-control" id="status">
                                <option>Active</option>
                                <option>Terminated</option>
                                <option>Deceased</option>
                                <option>Resigned</option>
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label for="comment" class="col-xs-4 col-sm-4 col-md-4 col-lg-4">Comment:</label>
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <textarea class="form-control" rows="3" id="comment"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- compensation Modal -->
<div class="modal fade" id="compensationModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Compensation</h4>
            </div>
            <div class="modal-body">
                <form class="text-left" action="">
                    <div class="row">
                        <label for="comDate" class="col-xs-4 col-sm-4 col-md-4 col-lg-4">Date:</label>
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <input type="date" class="form-control" id="comDate">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label for="comRate" class="col-xs-4 col-sm-4 col-md-4 col-lg-4">Pay Rate:</label>
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <input type="text" class="form-control" id="comRate">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label for="payType" class="col-xs-4 col-sm-4 col-md-4 col-lg-4">Employment status:</label>
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <select class="form-control" id="payType">
                                <option>Daily</option>
                                <option>Monthly</option>
                                <option>Yearly</option>
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label for="changeReason" class="col-xs-4 col-sm-4 col-md-4 col-lg-4">Change reason:</label>
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <select class="form-control" id="changeReason">
                                <option>Performance</option>
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label for="comComment" class="col-xs-4 col-sm-4 col-md-4 col-lg-4">Comment:</label>
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <textarea class="form-control" rows="3" id="comComment"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- compensation Modal -->
<div class="modal fade" id="jobInfoModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Job Information</h4>
            </div>
            <div class="modal-body">
                <form class="text-left" action="">
                    <div class="row">
                        <label for="infoDate" class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Date:</label>
                        <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                            <input type="date" class="form-control" id="infoDate">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label for="location" class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Location:</label>
                        <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                            <select class="form-control" id="location">
                                <option>Select</option>
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label for="department" class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Department:</label>
                        <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                            <select class="form-control" id="department">
                                <option>Management</option>
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label for="title" class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Job Title:</label>
                        <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                            <select class="form-control" id="title">
                                <option>Junior Manager</option>
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label for="to" class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Reporting to:</label>
                        <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                            <select class="form-control" id="to">
                                <option>Chathurangika</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
