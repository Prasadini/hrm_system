<div class="box no-top-border">
    <div class="box-header with-border">
        <h5 class="clr-black fnt-600 no-margin text-left"> General Info</h5>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-leftpad fnt-13">
            <table class="table table-responsive fnt-13 no-margin">
                <tbody>
                <tr>
                    <td class="width_25">First name :</td>
                    <td>Madhu</td>
                </tr>
                <tr>
                    <td class="width_25">Employee Id :</td>
                    <td>1005</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-leftpad fnt-13">
            <table class="table table-responsive fnt-13 no-margin">
                <tbody>
                <tr>
                    <td class="width_25">First name :</td>
                    <td>Madhu</td>
                </tr>
                <tr>
                    <td class="width_25">E-mail :</td>
                    <td>jhone@yahoo.com</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
<div class="box no-top-border collapsed-box">
    <div class="box-header with-border">
        <h5 class="clr-black fnt-600 no-margin text-left">Work</h5>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-leftpad fnt-13">
            <table class="table table-responsive fnt-13 no-margin">
                <tbody>
                <tr>
                    <td class="width_25">First name :</td>
                    <td>Madhu</td>
                </tr>
                <tr>
                    <td class="width_25">Employee Id :</td>
                    <td>1005</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-leftpad fnt-13">
            <table class="table table-responsive fnt-13 no-margin">
                <tbody>
                <tr>
                    <td class="width_25">First name :</td>
                    <td>Madhu</td>
                </tr>
                <tr>
                    <td class="width_25">E-mail :</td>
                    <td>jhone@yahoo.com</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
<div class="box no-top-border collapsed-box">
    <div class="box-header with-border">
        <h5 class="clr-black fnt-600 no-margin text-left"> Personal details</h5>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-leftpad fnt-13">
            <table class="table table-responsive fnt-13 no-margin">
                <tbody>
                <tr>
                    <td class="width_25">Address_1 :</td>
                    <td>sirimalwatta</td>
                </tr>
                <tr>
                    <td class="width_25">Address_2 :</td>
                    <td> -</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-leftpad fnt-13">
            <table class="table table-responsive fnt-13 no-margin">
                <tbody>
                <tr>
                    <td class="width_25">City :</td>
                    <td>Kandy</td>
                </tr>
                <tr>
                    <td class="width_25">State :</td>
                    <td>Kandy</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
<div class="box no-top-border collapsed-box">
    <div class="box-header with-border">
        <h5 class="clr-black fnt-600 no-margin text-left">Contact Details</h5>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-leftpad fnt-13">
            <table class="table table-responsive fnt-13 no-margin">
                <tbody>
                <tr>
                    <td class="width_25">Address_1 :</td>
                    <td>sirimalwatta</td>
                </tr>
                <tr>
                    <td class="width_25">Address_2 :</td>
                    <td> -</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-leftpad fnt-13">
            <table class="table table-responsive fnt-13 no-margin">
                <tbody>
                <tr>
                    <td class="width_25">City :</td>
                    <td>Kandy</td>
                </tr>
                <tr>
                    <td class="width_25">State :</td>
                    <td>Kandy</td>
                </tr>
                </tbody>
            </table>
        </div>
        <button class="btn btn-default btn-sm">Add Work experience +</button>
    </div>

</div>
<div class="box no-top-border collapsed-box">
    <div class="box-header with-border">
        <h5 class="clr-black fnt-600 no-margin text-left">Education</h5>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-leftpad fnt-13">
            <table class="table table-responsive fnt-13 no-margin">
                <tbody>
                <tr>
                    <td class="width_25">Address_1 :</td>
                    <td>sirimalwatta</td>
                </tr>
                <tr>
                    <td class="width_25">Address_2 :</td>
                    <td> -</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-leftpad fnt-13">
            <table class="table table-responsive fnt-13 no-margin">
                <tbody>
                <tr>
                    <td class="width_25">City :</td>
                    <td>Kandy</td>
                </tr>
                <tr>
                    <td class="width_25">State :</td>
                    <td>Kandy</td>
                </tr>
                </tbody>
            </table>
        </div>
        <button class="btn btn-default btn-sm">Add Work experience +</button>
    </div>

</div>
<div class="box no-top-border collapsed-box">
    <div class="box-header with-border">
        <h5 class="clr-black fnt-600 no-margin text-left">Dependants</h5>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-leftpad fnt-13">
            <table class="table table-responsive fnt-13 no-margin">
                <tbody>
                <tr>
                    <td class="width_25">Address_1 :</td>
                    <td>sirimalwatta</td>
                </tr>
                <tr>
                    <td class="width_25">Address_2 :</td>
                    <td> -</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-leftpad fnt-13">
            <table class="table table-responsive fnt-13 no-margin">
                <tbody>
                <tr>
                    <td class="width_25">City :</td>
                    <td>Kandy</td>
                </tr>
                <tr>
                    <td class="width_25">State :</td>
                    <td>Kandy</td>
                </tr>
                </tbody>
            </table>
        </div>
        <button class="btn btn-default btn-sm">Add Work experience +</button>
    </div>

</div>