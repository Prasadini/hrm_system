@extends('layout.master')
@section('content')
    @include('_includes.page_header')
    <div class="row" id="addNewEmployee">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">New Employee</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-3 col-lg-3 text-center">
                            <div style="margin:5px"><img src="img/user-placeholder.png" class="mx-auto d-block"
                                                         style="width:50%"></div>
                            <div style="margin:5px">
                                <button type="button" class="btn btn-info"><i class="material-icons icon">
                                        cloud_upload
                                    </i>upload Picture
                                </button>
                                <button type="button" class="btn btn-default">Cancel
                                </button>
                            </div>
                        </div>
                        <form class="col-md-9" style="border-left: 1px solid #868585;" @submit.prevent="addEmployee()">
                            {{--common details--}}
                            <div>

                                <div class="box-header with-border">
                                    <h3 class="box-title fnt-600">Common Details</h3>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="custom-form-group">
                                                <label for="first_name">First Name</label>
                                                <input type="text" class="custom-form-control" id="first_name"
                                                       v-model="form_data.first_name"
                                                       placeholder="Enter First Name">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="custom-form-group">
                                                <label for="middle_name">Middle Name</label>
                                                <input type="text" class="custom-form-control" id="middle_name"
                                                       v-model="form_data.middle_name"
                                                       placeholder="Enter >Middle Name">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="custom-form-group">
                                                <label for="last_name">Last Name</label>
                                                <input type="text" class="custom-form-control" id="last_name"
                                                       v-model="form_data.last_name"
                                                       placeholder="Last Name">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="custom-form-group">
                                                <label for="email_address">Email address</label>
                                                <input type="email" class="custom-form-control" id="email_address"
                                                       v-model="form_data.email_address"
                                                       placeholder="Enter email">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="custom-form-group">
                                                <label>Employee Type</label>
                                                <select class="custom-form-control" id="employee_type"
                                                        v-model="form_data.employee_type">
                                                    <option value="1">option 1</option>
                                                    <option value="2">option 2</option>
                                                    <option value="3">option 3</option>
                                                    <option value="4">option 4</option>
                                                    <option value="5">option 5</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="custom-form-group">
                                                <label>Employee Status</label>
                                                <select class="custom-form-control" id="employee_status"
                                                        v-model="form_data.employee_status">
                                                    <option value="1">option 1</option>
                                                    <option value="2">option 2</option>
                                                    <option value="3">option 3</option>
                                                    <option value="4">option 4</option>
                                                    <option value="5">option 5</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="custom-form-group">
                                                <label for="date_of_hire">Date of Hire</label>
                                                <input type="date" class="custom-form-control" id="date_of_hire"
                                                       v-model="form_data.date_of_hire"
                                                       placeholder="Enter email">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--work details--}}
                            <div v-if="add_work_details">
                                <div class="box-header with-border">
                                    <h3 class="box-title fnt-600">Work</h3>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="custom-form-group">
                                                <label>Department</label>
                                                <select class="custom-form-control" id="department"
                                                        v-model="work_form.department">
                                                    <option value="1">option 1</option>
                                                    <option value="1">option 2</option>
                                                    <option value="1">option 3</option>
                                                    <option value="1">option 4</option>
                                                    <option value="1">option 5</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="custom-form-group">
                                                <label>Job Title</label>
                                                <select class="custom-form-control" id="job_title"
                                                        v-model="work_form.job_title">
                                                    <option value="2">option 1</option>
                                                    <option value="2">option 2</option>
                                                    <option value="2">option 3</option>
                                                    <option value="2">option 4</option>
                                                    <option value="2">option 5</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="custom-form-group">
                                                <label>Location</label>
                                                <select class="custom-form-control" id="location"
                                                        v-model="work_form.location">
                                                    <option value="1">option 1</option>
                                                    <option value="1">option 2</option>
                                                    <option value="1">option 3</option>
                                                    <option value="1">option 4</option>
                                                    <option value="1">option 5</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="custom-form-group">
                                                <label>Reporting To</label>
                                                <select class="custom-form-control" id="reporting_to"
                                                        v-model="work_form.reporting_to">
                                                    <option value="1">option 1</option>
                                                    <option value="1">option 2</option>
                                                    <option value="1">option 3</option>
                                                    <option value="1">option 4</option>
                                                    <option value="1">option 5</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="custom-form-group">
                                                <label>Source of Hire</label>
                                                <select class="custom-form-control" id="source_of_hire"
                                                        v-model="work_form.source_of_hire">
                                                    <option value="1">option 1</option>
                                                    <option value="1">option 2</option>
                                                    <option value="1">option 3</option>
                                                    <option value="1">option 4</option>
                                                    <option value="1">option 5</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="custom-form-group">
                                                <label for="pay_rate">Pay Rate</label>
                                                <input type="text" step="0.01" class="custom-form-control"
                                                       id="pay_rate" v-model="work_form.pay_rate"
                                                       placeholder="Enter Pay Rate">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- /.box-body -->
                            </div>
                            {{--personnel details--}}
                            <div v-if="add_personal_details">
                                <div>
                                    <div class="box-header with-border">
                                        <h3 class="box-title fnt-600">Personal Information</h3>
                                    </div>
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="custom-form-group">
                                                    <label for="full_name">Name in Full</label>
                                                    <input type="text" class="custom-form-control"
                                                           id="full_name" v-model="personal_form.full_name"
                                                           placeholder="Enter Name in Full">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="custom-form-group">
                                                    <label for="name_in_full">Phone Number</label>
                                                    <div class="input-group">
                                                        <input type="text" class="custom-form-control" id="phone_number"
                                                               v-model="personal_form.phone_number">
                                                        <span class="input-group-addon" @click="addPhoneNumber()"><i
                                                                    class="material-icons icon"
                                                                    style="font-size: 12px">
                                                    add
                                                </i></span>
                                                    </div>
                                                </div>
                                                <ul class="contact-list">
                                                    <li v-for="(number,index) in personal_form.phone_numbers">
                                                        @{{ number }}
                                                        <a @click="removeContact(index)" style="color:red"
                                                           title="remove"> <i class="fa fa-minus"></i></a>
                                                    </li>
                                                </ul>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="custom-form-group">
                                                    <label for="secondary_mail">Secondary Email</label>
                                                    <input type="email" class="custom-form-control" id="secondary_mail"
                                                           v-model="personal_form.secondary_mail"
                                                           placeholder="Enter Secondary Email">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="custom-form-group">
                                                    <label for="dob">Date of Birth</label>
                                                    <input type="date" class="custom-form-control"
                                                           id="dob" v-model="personal_form.dob"
                                                           placeholder="Enter email">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="custom-form-group">
                                                    <label>Nationality</label>
                                                    <select class="custom-form-control" id="nationality"
                                                            v-model="personal_form.nationality">
                                                        <option value="1">option 1</option>
                                                        <option value="1">option 2</option>
                                                        <option value="1">option 3</option>
                                                        <option value="1">option 4</option>
                                                        <option value="1">option 5</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="custom-form-group">
                                                    <label>Gender</label>
                                                    <select class="custom-form-control" id="gender"
                                                            v-model="personal_form.gender">
                                                        <option value="1">option 1</option>
                                                        <option value="1">option 2</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="custom-form-group">
                                                    <label>Marital Status</label>
                                                    <select class="custom-form-control" id="marital_status"
                                                            v-model="personal_form.marital_status">
                                                        <option value="1">option 1</option>
                                                        <option value="1">option 2</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="custom-form-group">
                                                    <label for="nic_number">NIC Number</label>
                                                    <input type="text" class="custom-form-control"
                                                           id="nic_number" v-model="personal_form.nic_number"
                                                           placeholder="Enter NIC Number">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="custom-form-group">
                                                    <label for="address">Address:</label>
                                                    <input class="form-control"
                                                              id="address"
                                                              v-model="personal_form.address">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="custom-form-group">
                                                    <label for="city">City</label>
                                                    <input type="text" class="custom-form-control"
                                                           id="city" v-model="personal_form.city"
                                                           placeholder="Enter City">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="custom-form-group">
                                                    <label>Country</label>
                                                    <select class="custom-form-control" id="country"
                                                            v-model="personal_form.country">
                                                        <option value="1">option 1</option>
                                                        <option value="1">option 2</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="custom-form-group">
                                                    <label for="province">Province/State</label>
                                                    <input type="text" class="custom-form-control"
                                                           id="province" placeholder="Enter Province/State">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="custom-form-group">
                                                    <label for="postal_code">Postal code/Zip Code</label>
                                                    <input type="email" class="custom-form-control"
                                                           id="postal_code" placeholder="Enter Postal code/Zip Code">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="custom-form-group">
                                                    <label for="comment">Biography:</label>
                                                    <textarea class="form-control" rows="3" cols="5"
                                                              id="biography"
                                                              v-model="personal_form.biography"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <div>
                                    <div class="box-header with-border">
                                        <h3 class="box-title fnt-600">Contact in Emergency</h3><!-- tools box -->
                                        <div class="pull-right box-tools">
                                            <button type="button" class="btn btn-success btn-sm" title=""
                                                    @click="addEmployeeEmergency()">
                                                <i class="fa fa-plus"></i></button>
                                        </div>
                                        <!-- /. tools -->
                                    </div>
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-6 web-box-shadow"
                                                 v-for="(contact,index) in emergency_contacts">
                                                <label class="padding web-underline">Contact 01</label>
                                                <div class="custom-form-group padding">
                                                    <label for="guardian_name">Contact Name</label>
                                                    <input type="text" class="custom-form-control"
                                                           id="guardian_name" v-model="contact.name"
                                                           placeholder="Enter Full Name">
                                                </div>
                                                <div class="custom-form-group padding">
                                                    <label for="guardian_phone_no">Phone Number</label>
                                                    <input type="number" class="custom-form-control"
                                                           id="guardian_phone_no" v-model="contact.phone_no"
                                                           placeholder="Enter Phone Number">
                                                </div>
                                                <div class="custom-form-group padding">
                                                    <label for="guardian_address">Address</label>
                                                    <input type="email" class="custom-form-control"
                                                           id="guardian_address" v-model="contact.address"
                                                           placeholder="Enter Address">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="add_work_details"
                                                       v-model="add_work_details">
                                                Add work Details
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="add_personal_details"
                                                       v-model="add_personal_details">
                                                Add Personal Details
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="send_welcome_email"
                                                       v-model="form_data.send_welcome_email">
                                                Send the employee an welcome Email
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="send_login_details"
                                                       v-model="form_data.send_login_details">
                                                Send the login Details as well.
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer text-right">
                                <button type="submit" class="btn btn-info">Create Employee</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        let addNewEmployeeApp = new Vue({
            el: "#addNewEmployee",
            name: "addNewEmployeeApp",
            data: {
                add_work_details: false,
                add_personal_details: false,
                form_data: {
                    first_name: '',
                    middle_name: '',
                    last_name: '',
                    employee_id: '',
                    email_address: '',
                    employee_type: '',
                    employee_status: 0,
                    date_of_hire: '',
                    send_welcome_email: false,
                    send_login_details: false,
                },
                work_form: {
                    department: 0,
                    job_title: 0,
                    location: 0,
                    reporting_to: 0,
                    source_of_hire: 0,
                    pay_rate: 0,
                },
                personal_form: {
                    full_name: '',
                    phone_number: '',
                    phone_numbers: [],
                    secondary_email: '',
                    dob: '',
                    nationality: 0,
                    gender: 0,
                    marital_status: 0,
                    nic_number: '',
                    address: '',
                    city: '',
                    country: 0,
                    postal_code: 0,
                    province: 0,
                    biography: '',
                },
                emergency_contacts: [
                    {
                        name: '',
                        address: '',
                        phone_no: ''
                    },
                ]
            },
            computed: {},
            watch: {},
            methods: {
                addEmployee: function () {
                    let data = this.form_data;
                    data.work_form = this.work_form;
                    data.personal_form = this.personal_form;
                    data.add_work_details = this.add_work_details;
                    data.add_personal_details = this.add_personal_details;

                    this.$http.post('/create-new-employee', data).then((response) => {
                        if (response.data.success) {

                        } else {

                        }
                    });
                },
                addPhoneNumber: function () {
                    this.personal_form.phone_numbers.push(this.personal_form.phone_number);
                },
                removeContact: function (index) {
                    this.personal_form.phone_numbers.splice(index, 1);
                },
                addEmployeeEmergency: function () {
                    let obj = {
                        name: '',
                        address: '',
                        phone_no: ''
                    };
                    this.emergency_contacts.push(obj);
                }
            }
        })
    </script>
@endpush
<style>
    .contact-list {
        columns: 2;
        -webkit-columns: 2;
        -moz-columns: 2;
        list-style: none;
    }

    .web-box-shadow {
        box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.2), 0 1px 2px 0 rgba(0, 0, 0, 0.19);
    }

    .padding {
        padding: 5px;
    }

    .web-underline {
        text-decoration: underline;
    }
</style>