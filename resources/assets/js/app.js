/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');


var Vue = require('vue');


window.Vue = Vue;
Vue.use(require('vue-resource'));
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#csrf-token').getAttribute('content');


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
// require('./component');
// require('./global_functions');

require('./components');
Vue.component('example-component', require('./components/ExampleComponent.vue'));

/*import CONSTANTS from './constants';
import VeeValidate from 'vee-validate';
import ValidationErrors from './validation_errors';


window.CONSTANT =  CONSTANTS;

window.toastr =  require('toastr');
window.datetime = require('jquery-datetimepicker');

Vue.use(VeeValidate,ValidationErrors);*/


/*const app = new Vue({
    el: '#wrapper'
});*/
