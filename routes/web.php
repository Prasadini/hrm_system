<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\DB;



/*Route::get('/', function () {
    return view('welcome');
});*/


Route::get('/login', 'LoginController@showLogin');
Route::post('/user-login', 'LoginController@userLogin');
Route::get('/logout', 'LoginController@doLogout');

Route::group(['middleware' => ['authenticate']], function () {

    foreach (File::allFiles(__DIR__ . '/admin_routes') as $route_file) {
        require $route_file->getPathname();
    }

    Route::get('/', function () {
        return view('pages.dashboard');
    });

    Route::get('/employees', function () {
        return view('pages.employee.employee');
    });

    Route::get('/employee-profile', function () {
        return view('pages.employee.employee-profile');
    });

    Route::get('/ui-sample-test', function () {
        return view('pages.ui_sample_test');
    });

    Route::get('/data-entry', function () {
        $details = ['Manage Payroll', 'Manage Assets',
            'Manage Career Options', 'Manage Holidays', 'Manage Time Sheets'];

        foreach ($details as $key => $detail) {
            DB::table('side_menu_details')->insert([
                'menu_name' => $detail,
                'menu_detail' => $detail,
                'menu_master_id' => 0,
                'menu_level' => 1,
                'menu_url' => '/admin/' . str_slug($detail, '-'),
                'menu_icon' => '',
                'status' => 1,
                'sort_order' => (5 + $key + 1),
                'alias' => str_slug($detail, '-'),
            ]);
        }
    });

});

