<?php
/**
 * Created by PhpStorm.
 * User: Chaththa
 * Date: 6/24/2019
 * Time: 10:31 PM
 */

Route::resource('employee', 'EmployeeController', ['names' => [
    'index' => 'employee.index',
    'create' => 'employee.create',
    'show' => 'employee.show',
    'edit' => 'employee.edit',
    'store' => 'employee.store',
    'update' => 'employee.update',
    'destroy' => 'employee.destroy'
]]);

Route::post('/create-new-employee', 'EmployeeController@store');
Route::get('/add-new-employee', 'EmployeeController@index');


